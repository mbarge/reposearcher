//
//  repoTableViewCell.swift
//  RepoSearch
//
//  Created by Matthew Barge on 1/11/17.
//  Copyright © 2017 Matthew Barge. All rights reserved.
//

import UIKit

class repoTableViewCell: UITableViewCell {

    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var repoIDLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
