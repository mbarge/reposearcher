//
//  webViewVC.swift
//  RepoSearch
//
//  Created by Matthew Barge on 1/11/17.
//  Copyright © 2017 Matthew Barge. All rights reserved.
//

import UIKit

class webViewVC: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var webView: UIWebView!
    var urlString: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let frame = CGRect(x: 0,y: 0, width: containerView.bounds.width, height: containerView.bounds.height)
        webView.frame = frame
        loadRequest(urlStr: urlString)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadRequest(urlStr: String) {
        let url = NSURL(string: urlStr)
        let request = URLRequest(url: url! as URL)
        webView.loadRequest(request)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
