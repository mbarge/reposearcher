//
//  ViewController.swift
//  RepoSearch
//
//  Created by Matthew Barge on 1/11/17.
//  Copyright © 2017 Matthew Barge. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var githubURL = "https://api.github.com/search/repositories?q="
    var gitName = [String]()
    var gitID = [String]()
    var gitURL = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return gitName.count
}

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "repoTableViewCell", for: indexPath) as! repoTableViewCell
        
        cell.repoNameLabel.text = gitName[indexPath.row]
        cell.repoIDLabel.text = gitID[indexPath.row]
 
 // Configure the cell...
 
        return cell
 }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "webViewVC", sender: self)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if (searchBar.text?.isEmpty)! {
        } else {
        let finalURL = githubURL + searchBar.text! + "&page=1"
        downloadFromAPI(urlString: finalURL)
        view.endEditing(true)
        tableView.reloadData()
        }
    }

    func downloadFromAPI(urlString: String) {
        Alamofire.request(urlString, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let myJSON = JSON(value)
                for item in myJSON.arrayValue {
                    self.gitName.append(item["name"].stringValue)
                    self.gitID.append(item["id"].stringValue)
                    self.gitURL.append(item["html_url"].stringValue)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "webViewVC" {
            if let webViewVC = segue.destination as? webViewVC {
                let selectedRow = tableView.indexPathForSelectedRow?.row
                webViewVC.urlString = gitURL[selectedRow!]
            }
        }
        
    }

}

